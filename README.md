# Integration scenarios CoreA

This family of scenarios integrates components that support presentation of security offers to End-users and customization of SLA Templates. More information about integration scenarios can be found in deliverables D1.5.1 and D1.5.2.

### Core-A1
This scenario integrates the SLA Manager component (SLA Platform) and the SLO Manager component (Negotiation module) which provide basic functionalities for the creation and management of SLAs.

Details:
- Base Scenario ID: /
- Added artefacts: SLA Manager, SLO Manager

Involved components:
- SLA Platform:	SLA Manager
- Negotiation module: SLO Manager
- Enforcement module: /
- Monitoring module: /
- Vertical Layer: /
- Security mechanisms: /
- SPECS applications: /
