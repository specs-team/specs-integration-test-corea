package eu.specsproject.integration.util;

public class Common {
    public static String ipAddress = "localhost";
    
    private String SLO_MANAGER_BASE_PATH;
    private String SLO_MANAGER_TEMPLATE_PATH;
    private String TEMPLATE_NAME;
    private String TEMPLATE_PATH;
    
    static public String getSLO_MANAGER_BASE_PATH() {
        return "http://"+ipAddress+":8080/slo-manager-api";
    }

    static public String getSLO_MANAGER_TEMPLATE_PATH() {
        return getSLO_MANAGER_BASE_PATH()+"/sla-negotiation/sla-templates";
    }

    static public String getTEMPLATE_NAME() {
        return "SPECS_TEMPLATE_CCM_EXAMPLE";
    }

    static public String getTEMPLATE_PATH() {
        return "src/test/resources/SECURE_WEB_CONTAINER.xml";
    }

}
