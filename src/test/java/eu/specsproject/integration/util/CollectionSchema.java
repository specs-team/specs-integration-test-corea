package eu.specsproject.integration.util;

import java.util.List;

public class CollectionSchema {
    private String resource;
    private Integer total;
    private Integer members;
    private List<String> itemList;
    
    public CollectionSchema(){
        //Zero arguments constructor
    }
    
    public CollectionSchema(String resource, Integer total, Integer members, List<String> itemList){
        this.resource = resource;
        this.total = total;
        this.members = members;
        this.setItemList(itemList);
    }

    public String getResource() {
        return resource;
    }
    public void setResource(String resource) {
        this.resource = resource;
    }
    public Integer getTotal() {
        return total;
    }
    public void setTotal(Integer total) {
        this.total = total;
    }
    public Integer getMembers() {
        return members;
    }
    public void setItems(Integer members) {
        this.members = members;
    }
    public List<String> getItemList() {
        return itemList;
    }

    public void setItemList(List<String> itemList) {
        this.itemList = itemList;
    }

}