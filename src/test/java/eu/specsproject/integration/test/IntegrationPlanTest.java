package eu.specsproject.integration.test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.ws.rs.core.MediaType;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

import eu.specsproject.integration.util.Common;

public class IntegrationPlanTest{
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("setUpBeforeClass Called");
        String ipAddress = System.getenv("IP_ADDRESS");
        System.out.println("Ip Address: "+ipAddress);
        if(ipAddress != null){
            Common.ipAddress = ipAddress;
        }
        deleteTemplateFromSloManagerNoTest();
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("tearDownAfterClass Called");
    }

    @Before
    public void setUp() throws Exception {
        System.out.println("setUp Called");
    }
    
    @After
    public void tearDown() throws Exception {
        System.out.println("tearDown Called");
    }
    
    
    @Test
    public final void testUploadNewTemplate() {
        ClientConfig cc = new DefaultClientConfig();
        Client client = Client.create(cc);
        try {
            String nistTemplate = readFile(Common.getTEMPLATE_PATH());
            WebResource wr = client.resource(Common.getSLO_MANAGER_TEMPLATE_PATH());
            ClientResponse response = wr.type(MediaType.TEXT_XML).post(ClientResponse.class, nistTemplate);
            String resposeBody = response.getEntity(String.class);
            System.out.println(resposeBody);
            Assert.assertEquals(201, response.getStatus());
            Assert.assertEquals("\""+Common.getSLO_MANAGER_TEMPLATE_PATH()+"/"+Common.getTEMPLATE_NAME()+"\"", resposeBody);
            
            deleteTemplateFromSloManager(client, wr);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public final void testUploadExistentTemplate() {
        ClientConfig cc = new DefaultClientConfig();
        Client client = Client.create(cc);
        try {
            String nistTemplate = readFile(Common.getTEMPLATE_PATH());
            WebResource wr = client.resource(Common.getSLO_MANAGER_TEMPLATE_PATH());
            addTemplateToSloManager(wr, client);
            
            
            wr = client.resource(Common.getSLO_MANAGER_TEMPLATE_PATH());
            ClientResponse response = wr.type(MediaType.TEXT_XML).post(ClientResponse.class, nistTemplate);
            Assert.assertEquals(409, response.getStatus());

            deleteTemplateFromSloManager(client, wr);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    
    @Test
    public final void testRemoveExistentTemplate() {
        ClientConfig cc = new DefaultClientConfig();
        Client client = Client.create(cc);
        try {
            WebResource wr = client.resource(Common.getSLO_MANAGER_TEMPLATE_PATH());
            addTemplateToSloManager(wr, client);
            
            wr = client.resource(Common.getSLO_MANAGER_TEMPLATE_PATH()+"/"+Common.getTEMPLATE_NAME());
            ClientResponse response = wr.type(MediaType.TEXT_XML).delete(ClientResponse.class);
            Assert.assertEquals(204, response.getStatus());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public final void testRetrieveTemplatesCollection() {
        ClientConfig cc = new DefaultClientConfig();
        Client client = Client.create(cc);
        try {
            WebResource wr = client.resource(Common.getSLO_MANAGER_TEMPLATE_PATH());
            addTemplateToSloManager(wr, client);

            wr = client.resource(Common.getSLO_MANAGER_TEMPLATE_PATH());
            ClientResponse response = wr.get(ClientResponse.class);
            Assert.assertEquals(200, response.getStatus());
            String body = response.getEntity(String.class);
            Assert.assertTrue(body.contains(Common.getSLO_MANAGER_TEMPLATE_PATH()+"/"+Common.getTEMPLATE_NAME()));
            
            deleteTemplateFromSloManager(client, wr);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public final void testRetrieveTemplateBody() {
        ClientConfig cc = new DefaultClientConfig();
        Client client = Client.create(cc);
        try {
            WebResource wr = client.resource(Common.getSLO_MANAGER_TEMPLATE_PATH());
            addTemplateToSloManager(wr, client);

            //String nistTemplate = readFile(Common.TEMPLATE_PATH);
            wr = client.resource(Common.getSLO_MANAGER_TEMPLATE_PATH()+"/"+Common.getTEMPLATE_NAME());
            ClientResponse response = wr.get(ClientResponse.class);
            Assert.assertEquals(200, response.getStatus());
            //Assert.assertEquals(nistTemplate, response.getEntity(String.class));

            deleteTemplateFromSloManager(client, wr);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public final void testRetrieveTemplateBodyAndAddSlo() {
        ClientConfig cc = new DefaultClientConfig();
        Client client = Client.create(cc);
        try {
            WebResource wr = client.resource(Common.getSLO_MANAGER_TEMPLATE_PATH());
            addTemplateToSloManager(wr, client);

            wr = client.resource(Common.getSLO_MANAGER_TEMPLATE_PATH()+"/"+Common.getTEMPLATE_NAME());
            ClientResponse response = wr.post(ClientResponse.class);
            Assert.assertEquals(200, response.getStatus());

            deleteTemplateFromSloManager(client, wr);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    
    //UTILITIES FUNCTIONS
    private void addTemplateToSloManager(WebResource wr, Client client) throws IOException{
        String nistTemplate = readFile(Common.getTEMPLATE_PATH());
        wr = client.resource(Common.getSLO_MANAGER_TEMPLATE_PATH());
        ClientResponse response = wr.type(MediaType.TEXT_XML).post(ClientResponse.class, nistTemplate);
        String resposeBody = response.getEntity(String.class);
        System.out.println(resposeBody);
        Assert.assertEquals(201, response.getStatus());
        Assert.assertEquals("\""+Common.getSLO_MANAGER_TEMPLATE_PATH()+"/"+Common.getTEMPLATE_NAME()+"\"", resposeBody);
    }
    
    private void deleteTemplateFromSloManager(Client client, WebResource wr){
        wr = client.resource(Common.getSLO_MANAGER_TEMPLATE_PATH()+"/"+Common.getTEMPLATE_NAME());
        ClientResponse response = wr.type(MediaType.TEXT_XML).delete(ClientResponse.class);
        Assert.assertEquals(204, response.getStatus());
    }
    
    private static void deleteTemplateFromSloManagerNoTest(){
        ClientConfig cc = new DefaultClientConfig();
        Client client = Client.create(cc);
        WebResource wr = client.resource(Common.getSLO_MANAGER_TEMPLATE_PATH());
        wr = client.resource(Common.getSLO_MANAGER_TEMPLATE_PATH()+"/"+Common.getTEMPLATE_NAME());
        ClientResponse response = wr.type(MediaType.TEXT_XML).delete(ClientResponse.class);
        System.out.println("First delete response: "+response.getStatus());
    }
    
    private static String readFile(String fileName) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(fileName));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = br.readLine();
            }
            return sb.toString();
        } finally {
            br.close();
        }
    }
}
